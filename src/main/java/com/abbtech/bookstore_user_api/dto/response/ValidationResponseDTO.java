package com.abbtech.bookstore_user_api.dto.response;

import java.util.UUID;

public record ValidationResponseDTO(UUID userId, String userEmail) {
}

package com.abbtech.bookstore_user_api.dto.response;

public record AuthResponseDTO(
        String token
) {}
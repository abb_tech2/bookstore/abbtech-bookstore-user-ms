package com.abbtech.bookstore_user_api.dto.response;

public record
TokenResponse(String userId, String userEmail) {
}

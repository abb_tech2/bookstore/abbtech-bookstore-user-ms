package com.abbtech.bookstore_user_api.dto.response;

import com.abbtech.bookstore_user_api.dto.RoleDTO;

import java.util.Set;
import java.util.UUID;

public record UserResponseDTO(
        UUID id,
        String email,
        Set<RoleDTO> roles
) {
}

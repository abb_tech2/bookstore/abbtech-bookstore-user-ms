package com.abbtech.bookstore_user_api.dto.request;

public record ValidationRequestDto(String path, String method, String authHeader){
}

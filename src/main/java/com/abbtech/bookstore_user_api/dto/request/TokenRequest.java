package com.abbtech.bookstore_user_api.dto.request;

public record TokenRequest(String token) {
}

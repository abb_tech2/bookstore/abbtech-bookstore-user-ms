package com.abbtech.bookstore_user_api.dto.request;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;


public record RegisterRequestDTO (
    @NotBlank(message = "Firstname must not be null")
    String firstName,
    @NotBlank(message = "Lastname must not be null")
    String lastName,
    @Email(message = "Invalid email format")
    @NotBlank(message = "Email must not be null")
    String email,
    @NotBlank(message = "Password is required")
    @Size(min = 4, message = "Password must be at least 4 characters long")
    String password
){}

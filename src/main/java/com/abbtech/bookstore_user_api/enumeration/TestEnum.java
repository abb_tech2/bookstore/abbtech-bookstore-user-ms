package com.abbtech.bookstore_user_api.enumeration;

public enum TestEnum {
    // Admin permissions
    ADMIN_READ(MethodEnum.GET, "/users", RoleEnum.ADMIN),
    ADMIN_READ_BY_ID(MethodEnum.GET, "/users/{userId}", RoleEnum.ADMIN),
    ADMIN_READ_BOOK(MethodEnum.GET, "/books/{userId}", RoleEnum.ADMIN),

    // User permissions
    REGISTER(MethodEnum.POST, "/auth/register"),
    LOGIN(MethodEnum.POST, "/auth/login"),

    // Public access
    SWAGGER_API_DOCS(MethodEnum.GET, "/v3/api-docs/**"),
    SWAGGER_UI(MethodEnum.GET, "/swagger-ui/**"),

    // Authenticated user permissions
    CHANGE_PASSWORD(MethodEnum.POST, "/api/v1/auth/change-password"),
    CREATE_ORDER(MethodEnum.POST, "/orders"),
    VIEW_ORDER(MethodEnum.GET, "orders/{userId}"),
    VIEW_BOOK(MethodEnum.GET, "/books/{userId}"),
    VIEW_NOTIFICATIONS(MethodEnum.GET, "/notifications");

    private final MethodEnum method;
    private final String url;
    private final RoleEnum role;

    TestEnum(MethodEnum method, String url, RoleEnum role) {
        this.method = method;
        this.url = url;
        this.role = role;
    }

    TestEnum(MethodEnum method, String url) {
        this.method = method;
        this.url = url;
        this.role = null;
    }

    public boolean matches(String requestMethod, String requestUrl, RoleEnum userRole) {
        boolean methodMatches = method.equals(MethodEnum.valueOf(requestMethod));
        String urlPattern = url.replaceAll("\\{userId}", "[a-zA-Z0-9-]+");
        boolean urlMatches = requestUrl.matches(urlPattern);

        boolean roleMatches = role == null || role.equals(userRole);

        return methodMatches && urlMatches && roleMatches;
    }

    public static boolean isAuthorized(String requestMethod, String requestUrl, RoleEnum userRole) {
        for (TestEnum permission : TestEnum.values()) {
            if (permission.matches(requestMethod, requestUrl, userRole)) {
                return true;
            }
        }
        return false;
    }
}

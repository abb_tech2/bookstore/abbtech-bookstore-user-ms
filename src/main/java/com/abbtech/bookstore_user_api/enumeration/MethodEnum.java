package com.abbtech.bookstore_user_api.enumeration;

public enum MethodEnum {
    GET, PUT, DELETE, POST
}

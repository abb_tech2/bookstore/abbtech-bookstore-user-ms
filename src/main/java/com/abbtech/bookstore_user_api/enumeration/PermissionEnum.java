package com.abbtech.bookstore_user_api.enumeration;

import lombok.Getter;

@Getter
public enum PermissionEnum {

    ADMIN_PERMISSION_GET(new String[]{
            "/api/users",
            "/api/users/{id}",
            "/api/books",
            "/api/books/{id}"
    }, RoleEnum.ADMIN, MethodEnum.GET),

    ADMIN_PERMISSION_DELETE(new String[]{
            "/api/users/{id}",
            "/api/books/{id}"
    }, RoleEnum.ADMIN, MethodEnum.DELETE),

    ADMIN_PERMISSION_POST(new String[]{
            "/api/users",
            "/api/users/{id}",
            "/api/books/{id}"
    }, RoleEnum.ADMIN, MethodEnum.POST),

    PERMIT_ALL_PERMISSION_GET(new String[]{
            "/api/users/auth/register",
//            "/api/v3/api-docs/**",
            "/api/swagger-ui/**"
    }, MethodEnum.GET),

    PERMIT_ALL_PERMISSION_POST(new String[]{
            "/api/users/auth/register",
            "/api/users/auth/login",
            "/api/v3/api-docs/**",
            "/api/swagger-ui/**"
    }, MethodEnum.POST),

    LIST_URL_PERMISSION_GET(new String[]{
            "/api/orders/{id}",
            "/api/books/{id}",
            "/api/notifications"
    }, MethodEnum.GET),

    LIST_URL_PERMISSION_POST(new String[]{
            "/api/orders/{id}",
            "/api/books/{id}",
            "/api/notifications"
    }, MethodEnum.POST);

    private final String[] urls;
    private final RoleEnum role;
    private final MethodEnum method;

    PermissionEnum(String[] urls, RoleEnum role, MethodEnum method) {
        this.urls = urls;
        this.role = role;
        this.method = method;
    }

    PermissionEnum(String[] urls, MethodEnum method) {
        this.urls = urls;
        this.method = method;
        this.role = null;
    }

    public boolean matches(String requestMethod, String requestUrl, RoleEnum userRole) {
        // Check if the method matches
        boolean methodMatches = this.method.name().equals(requestMethod);
        if (methodMatches) {
            // Check if the URL matches any of the defined patterns
            for (String url : urls) {
                String urlPattern = url.replaceAll("\\{id\\}", "[a-zA-Z0-9-]+");
                if (requestUrl.matches(urlPattern)) {
                    // Check if the role matches or if the permission is role-independent
                    return role == null || role.equals(userRole);
                }
            }
        }

        return false;
    }

    public static boolean isAuthorized(String requestMethod, String requestUrl, RoleEnum userRole) {
        for (PermissionEnum permission : PermissionEnum.values()) {
            if (permission.matches(requestMethod, requestUrl, userRole)) {
                return true;
            }
        }
        return false;
    }

}

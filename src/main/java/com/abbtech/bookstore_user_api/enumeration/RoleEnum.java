package com.abbtech.bookstore_user_api.enumeration;

import lombok.Getter;

import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
public enum RoleEnum {

    USER(Collections.emptySet()),
    ADMIN(
            Set.of(
                    PermissionEnum.ADMIN_PERMISSION_GET,
                    PermissionEnum.ADMIN_PERMISSION_POST,
                    PermissionEnum.ADMIN_PERMISSION_DELETE
            )
    );

    private final Set<PermissionEnum> PermissionEnums;

    RoleEnum(Set<PermissionEnum> PermissionEnums) {
        this.PermissionEnums = PermissionEnums;
    }

//    public List<SimpleGrantedAuthority> getAuthorities() {
//        List<SimpleGrantedAuthority> authorities = PermissionEnums.stream()
//                .map(PermissionEnum -> new SimpleGrantedAuthority(PermissionEnum.name()))
//                .collect(Collectors.toList());
//        authorities.add(new SimpleGrantedAuthority("ROLE_" + this.name()));
//        return authorities;
//    }
}

package com.abbtech.bookstore_user_api.enumeration;

import lombok.Getter;

@Getter
public enum UrlMapping {

    ADMIN(RoleEnum.ADMIN.name(), new String[]{
            "/users",
            "users/{userId}",
            "/books/{{userId}}"
    }),

    PERMIT_ALL(new String[]{
            "/auth/register",
            "/auth/login",
            "/v3/api-docs/**",
            "/swagger-ui/**"
    }),

    ANY_AUTHENTICATED(new String[]{
            "/api/v1/auth/change-password",
            "/orders"
    }),

    LIST_URL(new String[]{
            "orders/{{userId}}",
            "/books/{{userId}}",
            "/notifications"
    });

    private final String role;
    private final String[] urls;

    UrlMapping(String role, String[] urls) {
        this.role = role;
        this.urls = urls;
    }

    UrlMapping(String[] urls) {
        this.role = null;
        this.urls = urls;
    }
}

package com.abbtech.bookstore_user_api.service.impl;

import com.abbtech.bookstore_user_api.config.JwtService;
import com.abbtech.bookstore_user_api.dto.request.TokenRequest;
import com.abbtech.bookstore_user_api.dto.request.ValidationRequestDto;
import com.abbtech.bookstore_user_api.enumeration.MethodEnum;
import com.abbtech.bookstore_user_api.enumeration.PermissionEnum;
import com.abbtech.bookstore_user_api.enumeration.RoleEnum;
import com.abbtech.bookstore_user_api.repository.UserRepository;
import com.abbtech.bookstore_user_api.service.ValidateService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@Slf4j
@RequiredArgsConstructor
public class ValidateServiceImpl implements ValidateService {
    private final JwtService jwtService;
    private final UserRepository userRepository;

    @Override
    public String parseUserIdFromToken(TokenRequest tokenRequest) {
        String token;
        String userId = null;
        String authHeader = tokenRequest.token();
        if (authHeader != null && authHeader.startsWith("Bearer ")) {
            token = authHeader.substring(7);
            userId = jwtService.extractUserId(token);
        }
        return userId;
    }

    @Override
    public String parseEmailFromToken(TokenRequest tokenRequest) {
        String token;
        String email = null;
        String authHeader = tokenRequest.token();
        if (authHeader != null && authHeader.startsWith("Bearer ")) {
            token = authHeader.substring(7);
            email = jwtService.extractEmail(token);
        }
        return email;
    }


    @Override
    public Boolean checkAuth(ValidationRequestDto validationRequestDto) {
        log.info("Checking the internal Auth");
        String authHeader = validationRequestDto.authHeader();
        String requestMethod = validationRequestDto.method();
        String requestUrl = validationRequestDto.path();

        if (authHeader != null && authHeader.startsWith("Bearer ")) {
            String token = authHeader.substring(7);
            String userId = jwtService.extractUserId(token);

            if (userId != null && jwtService.isTokenValid(token, userId)) {
                // Retrieve user details from repository
                var optionalUser = userRepository.findById(UUID.fromString(userId));
                if (optionalUser.isPresent()) {
                    var user = optionalUser.get();

                    // Check if the user has an admin role
                    boolean isAdmin = user.getRoles().stream()
                            .anyMatch(role -> role.getName() == RoleEnum.ADMIN);

                    // Set user role based on isAdmin flag
                    RoleEnum userRole = isAdmin ? RoleEnum.ADMIN : RoleEnum.USER;

                    // Check if the user has permission for the requested method and URL
                    return PermissionEnum.isAuthorized(requestMethod, requestUrl, userRole);
                }
            }
        }
        return false;
    }

    @Override
    public Boolean checkAccess(ValidationRequestDto validationRequestDto) {
        MethodEnum requestMethod = MethodEnum.valueOf(validationRequestDto.method());
        String requestUrl = validationRequestDto.path();

        for (PermissionEnum permission : PermissionEnum.values()) {
            if (permission.name().startsWith("PERMIT_ALL_PERMISSION_") && permission.getMethod().equals(requestMethod)) {
                for (String url : permission.getUrls()) {
                    String urlPattern = url.replaceAll("\\{id\\}", "[a-zA-Z0-9-]+");
                    if (requestUrl.matches(urlPattern)) {
                        return true; // Return true if there's a match
                    }
                }
            }
        }
        return false; // Return false if no match found
    }

}
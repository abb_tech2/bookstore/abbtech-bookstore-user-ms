package com.abbtech.bookstore_user_api.service.impl;

import com.abbtech.bookstore_user_api.dto.RoleDTO;
import com.abbtech.bookstore_user_api.dto.response.UserResponseDTO;
import com.abbtech.bookstore_user_api.exception.BadRequestException;
import com.abbtech.bookstore_user_api.exception.enums.BadRequestExceptionEnum;
import com.abbtech.bookstore_user_api.repository.UserRepository;
import com.abbtech.bookstore_user_api.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    @Override
    public List<UserResponseDTO> getAll() {
        return userRepository.findAll().stream()
                .map(user -> new UserResponseDTO(
                        user.getId(),
                        user.getEmail(),
                        user.getRoles().stream()
                                .map(role -> new RoleDTO(role.getName().name()))
                                .collect(Collectors.toSet())
                ))
                .toList();
    }

    @Override
    public UserResponseDTO getById(UUID id) {
        return userRepository.findById(id).map(user -> new UserResponseDTO(
                user.getId(),
                user.getEmail(),
                user.getRoles().stream()
                        .map(role -> new RoleDTO(role.getName().name()))
                        .collect(Collectors.toSet())
        )).orElseThrow(() -> new BadRequestException(BadRequestExceptionEnum.USER_NOT_FOUND));
    }
}

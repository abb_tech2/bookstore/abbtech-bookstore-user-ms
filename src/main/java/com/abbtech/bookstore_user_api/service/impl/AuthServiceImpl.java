package com.abbtech.bookstore_user_api.service.impl;

import com.abbtech.bookstore_user_api.config.JwtService;
import com.abbtech.bookstore_user_api.dto.request.LoginRequestDTO;
import com.abbtech.bookstore_user_api.dto.response.AuthResponseDTO;
import com.abbtech.bookstore_user_api.dto.request.RegisterRequestDTO;
import com.abbtech.bookstore_user_api.enumeration.RoleEnum;
import com.abbtech.bookstore_user_api.exception.BadRequestException;
import com.abbtech.bookstore_user_api.exception.enums.BadRequestExceptionEnum;
import com.abbtech.bookstore_user_api.model.Role;
import com.abbtech.bookstore_user_api.model.User;
import com.abbtech.bookstore_user_api.repository.RoleRepository;
import com.abbtech.bookstore_user_api.repository.UserRepository;
import com.abbtech.bookstore_user_api.service.AuthService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

@Service
@RequiredArgsConstructor
@Slf4j
public class AuthServiceImpl implements AuthService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final JwtService jwtService;

    @Transactional
    public AuthResponseDTO register(RegisterRequestDTO request) {
        log.info("Registering user with email: {}", request.email());

        if (userRepository.existsByEmail(request.email())) {
            throw new BadRequestException(BadRequestExceptionEnum.DUPLICATE_DATA);
        }

        Role defaultRole = roleRepository.findByName(RoleEnum.USER)
                .orElseThrow(() -> new BadRequestException(BadRequestExceptionEnum.ROLE_NOT_FOUND));

        String hashedPassword = BCrypt.hashpw(request.password(), BCrypt.gensalt());

        User user = User.builder()
                .firstName(request.firstName())
                .lastName(request.lastName())
                .email(request.email())
                .password(hashedPassword)
                .roles(Set.of(defaultRole))
                .build();

        userRepository.save(user);

        String jwtToken = jwtService.generateToken(user.getId().toString(), user.getEmail());
        log.info("User registered successfully: {}", request.email());

        return new AuthResponseDTO(jwtToken);
    }

    @Transactional
    public AuthResponseDTO login(LoginRequestDTO request) {
        log.info("Authenticating user: {}", request.email());

        User user = userRepository.findByEmail(request.email())
                .orElseThrow(() -> new BadRequestException(BadRequestExceptionEnum.USER_NOT_FOUND));

        if (!BCrypt.checkpw(request.password(), user.getPassword())) {
            throw new BadRequestException(BadRequestExceptionEnum.INVALID_CREDENTIALS);
        }

        String jwtToken = jwtService.generateToken(user.getId().toString(), user.getEmail());
        log.info("User authenticated successfully: {}", request.email());

        return new AuthResponseDTO(jwtToken);
    }
}

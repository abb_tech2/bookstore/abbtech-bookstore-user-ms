package com.abbtech.bookstore_user_api.service;

import com.abbtech.bookstore_user_api.dto.request.LoginRequestDTO;
import com.abbtech.bookstore_user_api.dto.response.AuthResponseDTO;
import com.abbtech.bookstore_user_api.dto.request.RegisterRequestDTO;



public interface AuthService {
    AuthResponseDTO register(RegisterRequestDTO request);

    AuthResponseDTO login(LoginRequestDTO request);

}

package com.abbtech.bookstore_user_api.service;

import com.abbtech.bookstore_user_api.dto.response.UserResponseDTO;

import java.util.List;
import java.util.UUID;

public interface UserService {
    List<UserResponseDTO> getAll();

    UserResponseDTO getById(UUID id);
}

package com.abbtech.bookstore_user_api.service;

import com.abbtech.bookstore_user_api.dto.request.TokenRequest;
import com.abbtech.bookstore_user_api.dto.request.ValidationRequestDto;

public interface ValidateService {
    String parseUserIdFromToken(TokenRequest parseTokenDto);

    String parseEmailFromToken(TokenRequest tokenRequest);

    Boolean checkAuth(ValidationRequestDto validationRequestDto);

    Boolean checkAccess(ValidationRequestDto validationRequestDto);
}

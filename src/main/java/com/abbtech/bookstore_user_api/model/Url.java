//package com.abbtech.bookstore_user_api.model;
//
//import jakarta.persistence.*;
//import lombok.*;
//
//import java.util.HashSet;
//import java.util.Set;
//import java.util.UUID;
//
//@Getter
//@Setter
//@Entity
//@Builder
//@AllArgsConstructor
//@NoArgsConstructor
//@Table(name = "urls", schema = "public")
//public class Url {
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private UUID id;
//
//    @Column(name = "url", nullable = false, unique = true)
//    private String url;
//
//    @ElementCollection(fetch = FetchType.EAGER)
//    @CollectionTable(name = "url_methods", joinColumns = @JoinColumn(name = "url_id"))
//    @Column(name = "method")
//    private Set<String> methods = new HashSet<>();
//
//    @ManyToMany(fetch = FetchType.EAGER)
//    @JoinTable(
//            name = "role_urls",
//            joinColumns = @JoinColumn(name = "url_id"),
//            inverseJoinColumns = @JoinColumn(name = "role_id")
//    )
//    private Set<Role> roles = new HashSet<>();
//}

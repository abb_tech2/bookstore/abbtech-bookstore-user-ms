//package com.abbtech.bookstore_user_api.model;
//
//import com.abbtech.bookstore_user_api.enumeration.PermissionEnum;
//import jakarta.persistence.*;
//import lombok.*;
//import org.springframework.security.core.GrantedAuthority;
//
//import java.util.Set;
//import java.util.UUID;
//
//@Getter
//@Setter
//@Entity
//@Builder
//@AllArgsConstructor
//@NoArgsConstructor
//@Table(name = "permissions", schema = "public")
//public class Permission implements GrantedAuthority {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private UUID id;
//
//    @Enumerated(EnumType.STRING)
//    @Column(name = "name", nullable = false, unique = true)
//    private PermissionEnum name;
//
//    @ManyToMany(mappedBy = "permissions")
//    private Set<Role> roles;
//
//    @Override
//    public String getAuthority() {
//        return name.getValue();
//    }
//}
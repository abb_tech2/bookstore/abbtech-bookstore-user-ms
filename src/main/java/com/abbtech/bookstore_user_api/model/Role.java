package com.abbtech.bookstore_user_api.model;

import com.abbtech.bookstore_user_api.enumeration.RoleEnum;
import jakarta.persistence.*;
import lombok.*;

import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "roles", schema = "public")
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private UUID id;

    @Enumerated(EnumType.STRING)
    @Column(name = "name", nullable = false, unique = true)
    private RoleEnum name;

//    @ManyToMany(fetch = FetchType.EAGER)
//    @JoinTable(
//            name = "role_permissions",
//            joinColumns = @JoinColumn(name = "role_id"),
//            inverseJoinColumns = @JoinColumn(name = "permission_id")
//    )
//    private Set<Permission> permissions;

    @ManyToMany(mappedBy = "roles")
    private Set<User> users;

//    public Set<SimpleGrantedAuthority> getAuthorities() {
//        Set<SimpleGrantedAuthority> authorities = permissions
//                .stream()
//                .map(permission -> new SimpleGrantedAuthority(permission.getAuthority()))
//                .collect(Collectors.toSet());
//        authorities.add(new SimpleGrantedAuthority("ROLE_" + name.name()));
//        return authorities;
//    }

}

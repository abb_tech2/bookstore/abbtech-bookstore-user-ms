package com.abbtech.bookstore_user_api.exception.enums;

import lombok.Getter;

@Getter
public enum BadRequestExceptionEnum {
    GENERAL_BUSINESS_ERROR("PROJECT-BIZ-0001", 400),
    USER_NOT_FOUND("PROJECT-BIZ-0002", 400),
    DEFINITION_NOT_ACCEPTABLE("PROJECT-BIZ-0003", 422),
    DUPLICATE_DATA("PROJECT-BIZ-0004", 400),
    ROLE_NOT_FOUND("PROJECT-BIZ-0005", 400),
    INVALID_CREDENTIALS("PROJECT-BIZ-0005", 400);
    //TODO 401, 402

    private final String errorCode;
    private final int statusCode;

    BadRequestExceptionEnum(String errorCode, int statusCode) {
        this.errorCode = errorCode;
        this.statusCode = statusCode;
    }
}

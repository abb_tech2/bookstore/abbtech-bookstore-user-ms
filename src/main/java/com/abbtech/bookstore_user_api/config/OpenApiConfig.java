package com.abbtech.bookstore_user_api.config;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;

@OpenAPIDefinition(
        info = @Info(
                contact = @Contact(
                        name = "Ulduz Safarli",
                        email = "ulduzsafarli.dv@gmail.com",
                        url = "https://gitlab.com/abb_tech2/bookstore"
                ),
                description = "User-MC",
                version = "1.0"
        )
)
public class OpenApiConfig {
}
package com.abbtech.bookstore_user_api.controller;

import com.abbtech.bookstore_user_api.dto.response.UserResponseDTO;
import com.abbtech.bookstore_user_api.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @GetMapping
    public List<UserResponseDTO> getAll() {
        return userService.getAll();
    }

    @GetMapping("/{id}")
    public UserResponseDTO getById(@PathVariable UUID id) {
        return userService.getById(id);
    }
}

package com.abbtech.bookstore_user_api.controller;

import com.abbtech.bookstore_user_api.dto.request.LoginRequestDTO;
import com.abbtech.bookstore_user_api.dto.response.AuthResponseDTO;
import com.abbtech.bookstore_user_api.dto.request.RegisterRequestDTO;
import com.abbtech.bookstore_user_api.service.AuthService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Validated
@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
@Tag(name = "Authentication Controller", description = "Endpoints for user authentication")
public class AuthController {
    private final AuthService authService;

    @Operation(summary = "User registration", description = "Register a new user")
    @PostMapping("/register")
    @ResponseStatus(HttpStatus.CREATED)
    public AuthResponseDTO register(@RequestBody @Valid RegisterRequestDTO userRegistrationDTO) {
        return authService.register(userRegistrationDTO);
    }

    @Operation(summary = "User login", description = "Authenticate user")
    @PostMapping("/login")
    public AuthResponseDTO login(@RequestBody @Valid LoginRequestDTO loginRequestDTO) {
        return authService.login(loginRequestDTO);
    }
}

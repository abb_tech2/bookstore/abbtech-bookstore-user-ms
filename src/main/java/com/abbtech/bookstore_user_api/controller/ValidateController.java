package com.abbtech.bookstore_user_api.controller;

import com.abbtech.bookstore_user_api.dto.request.TokenRequest;
import com.abbtech.bookstore_user_api.dto.request.ValidationRequestDto;
import com.abbtech.bookstore_user_api.dto.response.TokenResponse;
import com.abbtech.bookstore_user_api.service.ValidateService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//TODO swagger

@Validated
@RestController
@RequestMapping("/validate")
@RequiredArgsConstructor
public class ValidateController {

    private final ValidateService validateService;

    @PostMapping("/parseToken")
    public TokenResponse parseToken(@RequestBody TokenRequest tokenRequest){
        String userId = validateService.parseUserIdFromToken(tokenRequest);
        String email = validateService.parseEmailFromToken(tokenRequest);
        return new TokenResponse(email, userId);
    }

    @PostMapping("/checkAccess")
    public Boolean checkAccess(@RequestBody ValidationRequestDto validationRequestDto){
        return validateService.checkAccess(validationRequestDto);
    }

    @PostMapping("/checkAuth")
    public ResponseEntity<Boolean> checkAuth(@RequestBody ValidationRequestDto validationRequestDto) {
        if (validateService.checkAuth(validationRequestDto)) {
            return new ResponseEntity<>(true, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(false, HttpStatus.FORBIDDEN);
        }
    }

}

package com.abbtech.bookstore_user_api.repository;

import com.abbtech.bookstore_user_api.enumeration.RoleEnum;
import com.abbtech.bookstore_user_api.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface RoleRepository extends JpaRepository<Role, UUID> {
    Optional<Role> findByName(RoleEnum name);
}
